---?color=lavender
@title[Spring Batch]

@snap[south-north text-black]
## Spring Batch
@snapend

@snap[south docslink span-20]
[Spring Batch Documentation](https://spring.io/projects/spring-batch#overview)
@snapend

---?color=white
@title[Spring Batch Overview]

@snap[west span-35]
@box[bg-gold text-white box-wide-padding rounded](Apa itu Spring Batch?)
@snapend

@snap[east span-60 fragment]
@box[bg-green box-wide-padding rounded](Spring Batch merupakan framework yang mengakomudasi proses secara batch, dengan mengeksekusi serangkaian `job`)
@snapend

---?color=white
@title[Flow Spring Batch]

@snap[north span-80]
![](img/batch-diagram.jpg)
@snapend

@snap[south-west span-30 fragment]
@box[bg-orange text-white box-narrow-padding](Job terdiri dari serangkaian step)
@snapend

@snap[south span-30 fragment]
@box[bg-pink text-white box-narrow-padding](Step terdiri dari proses `Read`, `Process`, `Write`)
@snapend

@snap[south-east span-30 fragment]
@box[bg-purple text-white box-narrow-padding](Step juga dapat terdiri dari single operation atau istilah dalam spring batch disebut `Tasklet`)
@snapend

---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- Job : mendeskripsikan sebuah pekerjaan, misal nya membaca file `*.csv` dan kemudian menyimpan ke dalam database. Sebuah Job juga merupakan container dari satu atau beberapa kumpulan Step
- Step : merupakan bagian independent yang mengandung semua informasi terkait kontrol dari proses batch. Sebuah Job terdiri dari satu atau lebih Step, misalnya Step 1 untuk membaca file `*.csv` dan Step 2 adalah untuk mapping nilai file kedalam database
@snapend


---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- JobInstance : sebuah instance yang sedang berjalan dari sebuah Job yang telah ditetapkan dalam konfigurasi. Contoh Job untuk membaca file, artinya kita memiliki 1 JobInstance dengan kata lain 1 Job yang sedang berjalan sama dengan 1 JobInstance
- JobParameters : sekumpulan parameter yang digunakan oleh JobInstance
@snapend

---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- JobExecution : merupakan hasil dari menjalankan setiap JobInstance. Misal dalam membaca file `*.csv`, Job pertama mengalami kegagalan sedangkan berhasil setelah Job dijalankan ulang. Maka kita akan mempunyai 1 JobInstance dan memiliki 2 JobExecution (1 yang gagal, 1 yang berhasil)
- StepExecution : memiliki arti yang sama layaknya JobExecution namun lebih mempresentasikan hasil dari sebuah Step
@snapend


---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- JobRepository : sebuah persistent store untuk menyimpan semua informasi meta-data sebuah Job. Repository diperlukan untuk mengetahui state dari sebuah Job, apakah mengalami kegagalan atau tidak dalam proses nya. Secara default informasi disimpan pada memory, namun dapat diseting untuk disimpan dalam database
- JobLauncher : object yang memungkinkan kita untuk memulai sebuah Job menggunakan JobRepository untuk mendapatkan JobExecution yang valid
@snapend


---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- Tasklet : situasi dimana kita tidak memiliki input dan pengolahan dari output, misalnya ingin menjalankan/memanggil sebuah store procedure
- ItemReader : sebuah abstract class yang digunakan untuk menggambarkan sebuah object yang memungkinkan kita untuk membaca sebuah object yang ingin diproses, misalnya membaca file atau membaca data dari database
@snapend

---?image=img/green.jpg&position=center&size=100% 65%
@title[Istilah]

@snap[north span-100]
@size[1.5em](Beberapa Istilah Spring Batch)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- ItemProcessor : sebuah abstract class yang digunakan untuk menghandel bisnis logic dati data yang dihasilkan oleh ItemReader sebelum dilempar ke ItemWriter
- ItemWriter : sebuah abstract class yang digunakan untuk menulis hasil akhir dari proses batch
@snapend

---?color=lavender
@title[Spring Batch]

@snap[south-north text-black]
## Workshop
@snapend

@snap[south docslink span-20]
[Build & Run](https://gitlab.com/sharing-session-balicamp/Spring-Batch)
@snapend
