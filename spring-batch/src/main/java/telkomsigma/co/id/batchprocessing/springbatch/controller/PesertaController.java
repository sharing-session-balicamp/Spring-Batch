package telkomsigma.co.id.batchprocessing.springbatch.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 11:26
 */
@RestController
@RequestMapping("/peserta/process")
public class PesertaController {

    @Autowired private JobLauncher jobLauncher;
    @Autowired @Qualifier("importDataPesertaJob") private Job importDataPesertaJob;
    @Autowired @Qualifier("exportDataPesertaJob") private Job exportDataPesertaJob;

    @GetMapping("/import")
    public String executeJob() throws JobExecutionAlreadyRunningException, JobRestartException,
            JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        String status = "";
        try {
            JobExecution execution = jobLauncher.run(importDataPesertaJob, new JobParametersBuilder()
                    .addString("JobId", String.valueOf(System.currentTimeMillis()))
                    .toJobParameters());
            @SuppressWarnings("unused")
            Boolean isFailed = Boolean.FALSE;
            if (BatchStatus.FAILED.equals(execution.getStatus())) {
                isFailed = Boolean.TRUE;
                status = "FAILED!!!";
            } else {
                status = "SUCCESS!!";
            }
        } catch (Exception e) {
            status = "ERROR LAUNCH importDataPesertaJob : " + e.getMessage();
        }
        return status;
    }

    @GetMapping("/export")
    public String exportJob() throws JobExecutionAlreadyRunningException, JobRestartException,
            JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        String status = "";
        try {
            JobExecution execution = jobLauncher.run(exportDataPesertaJob, new JobParametersBuilder()
                    .addString("JobId", String.valueOf(System.currentTimeMillis()))
                    .toJobParameters());
            @SuppressWarnings("unused")
            Boolean isFailed = Boolean.FALSE;
            if (BatchStatus.FAILED.equals(execution.getStatus())) {
                isFailed = Boolean.TRUE;
                status = "FAILED!!!";
            } else {
                status = "SUCCESS!!";
            }
        } catch (Exception e) {
            status = "ERROR LAUNCH exportDataPesertaJob : " + e.getMessage();
        }
        return status;
    }

}
