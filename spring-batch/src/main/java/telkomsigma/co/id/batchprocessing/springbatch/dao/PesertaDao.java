package telkomsigma.co.id.batchprocessing.springbatch.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:19
 */
public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {
}
