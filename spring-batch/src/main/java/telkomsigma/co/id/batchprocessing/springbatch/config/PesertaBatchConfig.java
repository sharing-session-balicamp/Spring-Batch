package telkomsigma.co.id.batchprocessing.springbatch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;
import telkomsigma.co.id.batchprocessing.springbatch.listener.SkipChekingListener;
import telkomsigma.co.id.batchprocessing.springbatch.mapper.PesertaMapper;
import telkomsigma.co.id.batchprocessing.springbatch.processor.PesertaItemProcessor;
import telkomsigma.co.id.batchprocessing.springbatch.writer.PesertaItemWriter;

import java.sql.SQLDataException;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:22
 */
@Configuration
public class PesertaBatchConfig {
    @Autowired public JdbcTemplate jdbcTemplate;
    @Autowired public JobBuilderFactory jobBuilderFactory;
    @Autowired public StepBuilderFactory stepBuilderFactory;

    @Autowired public PesertaItemProcessor processor;
    @Autowired public PesertaItemWriter itemWriter;
    @Autowired public SkipChekingListener skipChekingListener;

    private static final Logger LOG = LoggerFactory.getLogger(PesertaBatchConfig.class);

    @Bean
    public FlatFileItemReader<Peserta> reader() {
        FlatFileItemReader<Peserta> reader = new FlatFileItemReader<Peserta>();
        reader.setResource(new ClassPathResource("test-data.csv"));
        reader.setLineMapper(new DefaultLineMapper<Peserta>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {
                        setNames(new String[] { "nama", "alamat", "tanggalLahir" });
                    }
                });
                setFieldSetMapper(new PesertaMapper());
            }
        });

        return reader;
    }

    @Bean
    public Step importPesertaStep() {
        return stepBuilderFactory.get("step-1")
                .<Peserta, Peserta>chunk(1000)
                .reader(reader())
                .processor(processor)
                .writer(itemWriter)
                .faultTolerant()
                .skip(FlatFileParseException.class)
                .skip(SQLDataException.class)
                .skipLimit(2)
                .retry(SQLDataException.class)
                .retryLimit(3)
                .listener(skipChekingListener)
                .build();
    }

    @Bean
    public Job importDataPesertaJob() {
        return jobBuilderFactory
                .get("importPesertaJob")
                .incrementer(new RunIdIncrementer())
                .flow(importPesertaStep())
                .end()
                .build();
    }

}