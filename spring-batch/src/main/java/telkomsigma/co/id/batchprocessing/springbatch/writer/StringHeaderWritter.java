package telkomsigma.co.id.batchprocessing.springbatch.writer;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

import java.io.IOException;
import java.io.Writer;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 16:32
 */
public class StringHeaderWritter implements FlatFileHeaderCallback {
    private final String header;

    public StringHeaderWritter(String header) {
        this.header = header;
    }

    @Override
    public void writeHeader(Writer writer) throws IOException {
        writer.write(header);
    }
}
