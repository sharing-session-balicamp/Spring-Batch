package telkomsigma.co.id.batchprocessing.springbatch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;
import telkomsigma.co.id.batchprocessing.springbatch.writer.StringHeaderWritter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 16:29
 */
@Configuration
public class ExportBatchConfig {
    @Autowired public JobBuilderFactory jobBuilderFactory;
    @Autowired public StepBuilderFactory stepBuilderFactory;
    @Autowired public DataSource dataSource;

    @Bean
    public ItemReader<Peserta> databaseItemReader(){
        JdbcPagingItemReader<Peserta> data = new JdbcPagingItemReader<Peserta>();
        data.setDataSource(dataSource);
        data.setPageSize(1);
        data.setRowMapper(new BeanPropertyRowMapper<>(Peserta.class));
        data.setQueryProvider(createQueryProvider());
        return data;
    }

    private PagingQueryProvider createQueryProvider() {
        MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();
        queryProvider.setSelectClause("SELECT * ");
        queryProvider.setFromClause("FROM peserta");
        queryProvider.setSortKeys(sortByIdAsc());
        return queryProvider;
    }

    private Map<String, Order> sortByIdAsc() {
        Map<String, Order> sort = new HashMap<>();
        sort.put("id", Order.ASCENDING);
        return sort;
    }

    @Bean
    public ItemWriter<Peserta> dbToCsvWritter(){
        FlatFileItemWriter<Peserta> csvWritter = new FlatFileItemWriter<>();
        String exportFileHeader = "id;nama;alamat;tanggalLahir";
        csvWritter.setHeaderCallback(new StringHeaderWritter(exportFileHeader));
        csvWritter.setResource(new FileSystemResource("~/Documents/export-data.csv"));
        //csvWritter.setResource(new ClassPathResource("export-data.csv"));
        csvWritter.setLineAggregator(createLineAggregator());
        return csvWritter;
    }

    private LineAggregator<Peserta> createLineAggregator() {
        DelimitedLineAggregator<Peserta> delimitedLineAggregator = new DelimitedLineAggregator<Peserta>();
        delimitedLineAggregator.setDelimiter(";");
        delimitedLineAggregator.setFieldExtractor(createFieldExtractor());
        return delimitedLineAggregator;
    }

    private FieldExtractor<Peserta> createFieldExtractor() {
        BeanWrapperFieldExtractor<Peserta> extractor = new BeanWrapperFieldExtractor<>();
        extractor.setNames(new String[] {"id", "name", "alamat", "tanggalLahir"});
        return extractor;
    }

    @Bean
    public Step dbToCsvStep(ItemReader<Peserta> itemReader, ItemWriter<Peserta> itemWritter) {
        return stepBuilderFactory.get("dbToCsvStep")
                .<Peserta, Peserta>chunk(10)
                .reader(itemReader)
                .writer(itemWritter)
                .build();
    }

    @Bean
    public Job exportDataPesertaJob() {
        return jobBuilderFactory.get("exportDataPesertaJob")
                .incrementer(new RunIdIncrementer())
                .flow(dbToCsvStep(databaseItemReader(), dbToCsvWritter()))
                .end()
                .build();
    }
}
