package telkomsigma.co.id.batchprocessing.springbatch.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:08
 */
@Data
@Entity @Table(name = "Peserta")
public class Peserta {
    @Id
    @GeneratedValue(generator="uuid") @GenericGenerator(name="uuid", strategy="uuid2")
    private String id;
    private String name;
    private String alamat;
    @Temporal(TemporalType.DATE)
    private Date tanggalLahir;

}
