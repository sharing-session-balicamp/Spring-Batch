package telkomsigma.co.id.batchprocessing.springbatch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:31
 */
@Component
public class PesertaItemProcessor implements ItemProcessor<Peserta, Peserta> {

    @Override
    public Peserta process(Peserta peserta) throws Exception {
        String nama = peserta.getName().toUpperCase();
        Peserta newPeserta = new Peserta();
        newPeserta.setName(nama);
        newPeserta.setAlamat(peserta.getAlamat());
        newPeserta.setTanggalLahir(peserta.getTanggalLahir());

        return newPeserta;
    }
}
