package telkomsigma.co.id.batchprocessing.springbatch.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:26
 */
@Component
public class PesertaMapper implements FieldSetMapper<Peserta> {
    @Override
    public Peserta mapFieldSet(FieldSet fs) throws BindException {
        Peserta peserta = new Peserta();
        peserta.setName(fs.readRawString(0));
        peserta.setAlamat(fs.readString("alamat"));
        peserta.setTanggalLahir(fs.readDate(2));

        return peserta;
    }
}
