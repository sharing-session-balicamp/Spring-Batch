package telkomsigma.co.id.batchprocessing.springbatch.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telkomsigma.co.id.batchprocessing.springbatch.dao.PesertaDao;
import telkomsigma.co.id.batchprocessing.springbatch.entity.Peserta;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-04-05
 * Time: 10:28
 */
@Component
public class PesertaItemWriter implements ItemWriter<Peserta> {

    private static final Logger LOG = LoggerFactory.getLogger(PesertaItemWriter.class);

    @Autowired
    private PesertaDao pesertaDao;

    @Override
    public void write(List<? extends Peserta> list) throws Exception {
        for(Peserta p : list) {

            LOG.info("PESERTA YANG AKAN DI SAVE : {}",p.getName());
            pesertaDao.save(p);
        }
    }

}
